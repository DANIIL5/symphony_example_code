<?php
// tests/Util/CalculatorTest.php
namespace App\Tests\Util;

use App\Util\Calculator;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CalculatorTest extends WebTestCase
{
    public function testAdd()
    {
        $client = static::createClient();

        $calculator = new Calculator();
        //$result = $calculator->add(30, 12);
        //$client = new Client();
        //$client->request('GET', 'http://127.0.0.1:8008/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713010,79250713019&cost=3&err=1&all=1&mes=texttextte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35');
        //$res = $client->request('GET', 'http://127.0.0.1:8008/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713010,79250713019&cost=3&err=1&all=1&mes=texttextte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35', ['user-agent', ['http_errors' => false]

       // ]);
       // $res1 = $client->request('GET', 'http://127.0.0.1:8008/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713011,79250713012&cost=3&err=1&all=1&mes=texttextte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35', [

       // ]);

        // assert that your calculator added the numbers correctly!

        // asserts that the response status code is 2xx
       // $this->assertTrue($client->getResponse()->isSuccessful(), 'response status is 2xx');

        // asserts a specific 200 status code
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713012&cost=3&err=1&all=1&mes=texttextte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713012&cost=3&err=1&all=1&mes=texttextte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713012&cost=3&err=1&all=1&mes=texttextte345xt11111&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713012&cost=3&err=1&all=1&mes=texttex213213tte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713012,79250713012&cost=3&err=1&all=1&mes=texttex213213tte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713015,79250713012&cost=3&err=1&all=1&mes=texttex213213tte345xt&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713015,79250713012&cost=3&err=1&all=1&mes=texttex&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713015,79250713012&cost=3&err=1&all=1&mes=textt2ex&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713015,79250713012&cost=3&err=1&all=1&mes=text2t2ex&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713017,79250713012&cost=3&err=1&all=1&mes=text2t2ex&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713018,79250713018&cost=3&err=1&all=1&mes=text2t2ex&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        $client->request('GET', 'http://127.0.0.1:8009/?login=d.bessonov&psw=YCxdu9e5GJ&fmt=3&charset=utf-8&phones=79250713019,79250713019&cost=3&err=1&all=1&mes=text2t2ex&id=c159e8a8b6154e6fad9433cdce2b4e35&sender=INCITY');
        $this->assertEquals(
            200, // or Symfony\Component\HttpFoundation\Response::HTTP_OK
            $client->getResponse()->getStatusCode()
        );
        //$this->assertTrue($client->getResponse()->isSuccessful(), 'response status is 4xx');
       // $this->assertEquals(200, $res1->getStatusCode());

    }
}