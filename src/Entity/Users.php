<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="idx_24354_login", columns={"login"})}, indexes={@ORM\Index(name="idx_24354_rs_id", columns={"rs_id"}), @ORM\Index(name="idx_24354_routing_group_id", columns={"routing_group_id"}), @ORM\Index(name="idx_24354_smpp_user", columns={"smpp_user"}), @ORM\Index(name="IDX_1483A5E919EB6921", columns={"client_id"})})
 * @ORM\Entity
 */
class Users

{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="users_id_seq", allocationSize=1, initialValue=1)
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="rs_id", type="bigint", nullable=false)
     */


    /**
     * @var string
     *
     * @ORM\Column(name="login", type="text", nullable=false)
     */
    private $login = '';

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="text", nullable=false)
     */
    private $password = '';

    /**
     * @var int
     *
     * @ORM\Column(name="unlim_send", type="integer", nullable=false)
     */

    /**
     * @var int|null
     *
     * @ORM\Column(name="users_api_id", type="integer", nullable=true)
     */
    private $usersApiId;



    public function getId(): ?int
    {
        return $this->id;
    }


    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }



    public function getUsersApiId(): ?int

    {
        return $this->usersApiId;
    }

    public function setUsersApiId(?int $usersApiId): self

    {
        $this->usersApiId = $usersApiId;

        return $this;
    }






}
