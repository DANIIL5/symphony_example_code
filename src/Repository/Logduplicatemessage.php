<?php

namespace App\Repository;

use App\Entity\Log_duplicate_message;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Log_duplicate_message|null find($id, $lockMode = null, $lockVersion = null)
 * @method Log_duplicate_message|null findOneBy(array $criteria, array $orderBy = null)
 * @method Log_duplicate_message[]    findAll()
 * @method Log_duplicate_message[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Logduplicatemessage extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Log_duplicate_message::class);
    }

    // /**
    //  * @return MessageFilter[] Returns an array of MessageFilter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MessageFilter
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
