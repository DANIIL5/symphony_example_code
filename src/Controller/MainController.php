<?php

namespace App\Controller;
use App\Entity\Log_duplicate_message;
use App\Entity\Users;
use Redis;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Cache\Adapter\RedisAdapter;

class MainController extends AbstractController
{

    /**
     * @Route("/requests/checkdm_sendsms", name="homepage")
     */
    public function index(Request $request)
    {

        $redis = new \Predis\Client();
        $params=$request->query->all();
        if(empty($params)){

            $error['error']='you have not passed any parameters';

            $errors=json_encode($error,JSON_UNESCAPED_UNICODE);

            $response = new Response($errors);

            $response->headers->set('Content-Type', 'application/json');

            return $response->setStatusCode('400');
        }


        $phones=$request->get('phones');

        $mes=$request->get('mes');

        $phone=explode(',',$phones);

        $flags_keys=false;
        $flags_error=false;
        foreach ($phone as $one_phone) {

            $keys=$redis->keys($one_phone.':'.$params['login']);
            if(isset($keys)){

                $flags_keys=true;

            }
        }
        $eror='';
        if($flags_keys==true && (!empty($params['login']) && !empty($params['psw']) )){
        
            $search_user = $this->getDoctrine()->getRepository(Users::class)->findBy(['login'=>$params['login'],'password'=>md5($params['psw']),'usersApiId'=>5]);
            
            if(empty($search_user)){
                $error['error']='Неверный логин или пароль или нет прав';

                $errors=json_encode($error,JSON_UNESCAPED_UNICODE);

                $response = new Response($errors);

                $response->headers->set('Content-Type', 'application/json');

                return $response->setStatusCode('400');

            }

                if(count($phone)>1){
                    foreach ($phone as $one_phone) {
                        $phone_message=$redis->smembers($one_phone.':'.$params['login']);
                        foreach ($phone_message as $one_message) {
                            if($mes===$one_message){
                                $error['error'][]='This message on number '.$one_phone.' already exists';
                                // you can fetch the EntityManager via $this->getDoctrine()
                                // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
                                if((!empty($params['login'])) && !empty($params['psw']) && !empty($params['sender']) && !empty($params['phones'])  && !empty($params['mes']) && !empty($params['id']) && (!empty($params['charset']) && $params['charset']==='utf-8')){
                                    $entityManager = $this->getDoctrine()->getManager();
                                    $phone_mes = new Log_duplicate_message();
                                    $phone_mes->setSender($params['sender']);
                                    $phone_mes->setText($params['mes']);
                                    $phone_mes->setPhone($one_phone);
                                    $phone_mes->setLogin($params['login']);
                                    $phone_mes->setCreatedAt(new \DateTime());
                                    // tell Doctrine you want to (eventually) save the Product (no queries yet)
                                    $entityManager->persist($phone_mes);
                                    // actually executes the queries (i.e. the INSERT query)
                                    $entityManager->flush();
                                }

                                $flags_error=true;

                            }


                        }
                    }
                }
                else{

                   $phone_message=$redis->smembers($phone[0].':'.$params['login']);
                   foreach ($phone_message as $one_message) {
                       if($mes===$one_message){
                           $error['error']='This message on number '.$phone[0].' already exists';
                           if((!empty($params['login'])) && !empty($params['psw']) && !empty($params['sender']) && !empty($params['phones'])  && !empty($params['mes']) && !empty($params['id']) && (!empty($params['charset']) && $params['charset']==='utf-8')) {
                               // you can fetch the EntityManager via $this->getDoctrine()
                               // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
                               $entityManager = $this->getDoctrine()->getManager();
                               $phone_mes = new Log_duplicate_message();
                               $phone_mes->setSender($params['sender']);
                               $phone_mes->setText($params['mes']);
                               $phone_mes->setPhone($phone[0]);
                               $phone_mes->setLogin($params['login']);
                               $phone_mes->setCreatedAt(new \DateTime());
                               // tell Doctrine you want to (eventually) save the Product (no queries yet)
                               $entityManager->persist($phone_mes);
                               // actually executes the queries (i.e. the INSERT query)
                               $entityManager->flush();
                               $flags_error = true;
                           }

                       }

                   }

                }
                
        }


        if(!$flags_error){
        
            if((!empty($params['login'])) && !empty($params['psw']) && !empty($params['sender']) && !empty($params['phones'])  && !empty($params['mes']) && !empty($params['id']) && (!empty($params['charset']) && $params['charset']==='utf-8')){
                foreach ($phone as $one_phone) {

                    $redis->sadd($one_phone.':'.$params['login'] , $mes);
                    $redis->expire($one_phone ,86400);

                }

            }
            try {
            
            $getdata = http_build_query($params);

            $opts  = array(
                'https' => array (
                    'method'  => 'GET',
                    'content' => $getdata
                )
            );
            
            $context  = stream_context_create($opts);

            $result=file_get_contents('https://xml.smstec.ru/requests/incity_sendsms?'.$getdata, false, $context);

            $response = new Response($result);

            $response->headers->set('Content-Type', 'application/json');


            } 
            catch (\Exception $e) {
            
                $error['error']='503 Service Temporarily Unavailable';

                $errors=json_encode($error,JSON_UNESCAPED_UNICODE);

                $response = new Response($errors);

                return $response;
            }

            return $response;

        }
        
        else{

            $errors=json_encode($error,JSON_UNESCAPED_UNICODE);

            $response = new Response($errors);

            $response->headers->set('Content-Type', 'application/json');

            return $response->setStatusCode('400');
        }


    }
}
